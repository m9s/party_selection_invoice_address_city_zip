#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Party Selection Invoice Address City Zip',
    'name_de_DE': 'Partei Auswahl Postleitzahl/Ort der Rechnungsadresse',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''
    - Allows the selection of a party by city or zip of the invoice address
    ''',
    'description_de_DE': '''
    - Ermöglicht die Auswahl von Parteien nach Ort oder Postleitzahl
      der Rechnungsadresse
''',
    'depends': [
        'party',
        'account_invoice'
    ],
    'xml': [
        'party.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
