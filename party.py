#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool


class Party(ModelSQL, ModelView):
    _name = 'party.party'

    city = fields.Function(fields.Char('City'), 'get_address_detail_city',
            searcher='search_address_detail')
    zip = fields.Function(fields.Char('Zip'), 'get_address_detail_zip',
            searcher='search_address_detail')

    def get_address_detail_city(self, ids, name):
        arg='city'
        return self._get_address_detail(ids, name, arg)

    def get_address_detail_zip(self, ids, name):
        arg='zip'
        return self._get_address_detail(ids, name, arg)

    def _get_address_detail(self, ids, name, arg):
        res = {}
        if not ids:
            return []

        for party in self.browse(ids):
            res[party.id] = ''
            for address in party.addresses:
                if address.invoice:
                    res[party.id] = eval('address.'+arg)
                    break
            if not res[party.id] and party.addresses:
                    res[party.id] = eval('party.addresses[0].'+arg)
        return res

    def search_address_detail(self, name, clause):
        address_obj = Pool().get('party.address')
        res = []
        address_ids = address_obj.search(clause)
        for address in address_obj.browse(address_ids):
            res.append(address.party.id)
        return [('id','in', res)]

Party()
